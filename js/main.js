//Task 1

const boardItem = document.querySelectorAll('.board__item'),
      boardMessage = document.querySelector('.board__message');

const showBoardMessage = () => {
    boardMessage.style.display = 'flex';
    boardMessage.classList.add('animated',  'fadeInRight')
};

const getRandomColor = (min = 0, max = 255) => {
    return Math.floor(Math.random() * (max - min) + min);
};

const getRandomElement = () => {
    return boardItem[Math.floor(Math.random() * boardItem.length)];
};


const addElementOnBoard = () => {
    let randomItem = getRandomElement();
    randomItem.style.backgroundColor = `rgba(${getRandomColor()},${getRandomColor()},${getRandomColor()},1)`;
};


const interval = setInterval(() => {
    let emptyArr = [];
    addElementOnBoard();

    boardItem.forEach(item => {

        if(item.hasAttribute('style')) {
            emptyArr.push(item);
        }

        if(emptyArr.length === 100) {
            clearInterval(interval);
            showBoardMessage();
       }
    }); 

},10);






//Task 2


// const input = document.querySelector('.container__input'),
//       btn = document.querySelector('.container__btn');

// const mathExp = ['2+2=','5-2=', '10/2='];

// let counter = 0;
